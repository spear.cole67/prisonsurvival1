using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingSystem : MonoBehaviour
{

    public GameObject craftingScreenUI;
    public GameObject toolsScreenUI;
    public int rock_count = 0;
    public int branch_count = 0;
    public List<string> inventoryItemList = new List<string>();

    // Category Buttons
    Button toolsBTN;

    // Craft Buttons
    Button craftAxeBTN;

    // Crafting Requirements
    Text AxeReq1, AxeReq2;

    public bool isOpen;

    // Blueprints
    public Blueprint Axe_BP = new Blueprint("Axe", "Rock", "Branch", 3, 3, 2);

    public static CraftingSystem Instance { get; set; }

    private void Awake()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
        toolsBTN = craftingScreenUI.transform.Find("ToolsButton").GetComponent<Button>();
        toolsBTN.onClick.AddListener(delegate { OpenToolsCategory(); });

        // Axe
        AxeReq1 = toolsScreenUI.transform.Find("Axe").transform.Find("AxeReq1").GetComponent<Text>();
        AxeReq2 = toolsScreenUI.transform.Find("Axe").transform.Find("AxeReq2").GetComponent<Text>();

        craftAxeBTN = toolsScreenUI.transform.Find("Axe").transform.Find("craftAxeButton").GetComponent<Button>();
        craftAxeBTN.onClick.AddListener(delegate { craftItem(Axe_BP); });
    }

    void OpenToolsCategory()
    {
        craftingScreenUI.SetActive(false);
        toolsScreenUI.SetActive(true);
    }

    void craftItem(Blueprint bp)
    {
        InventorySystem.Instance.RemoveItem(bp.req1, bp.req1Amount);
        if (bp.reqAmount > 1) InventorySystem.Instance.RemoveItem(bp.req2, bp.req2Amount);
        InventorySystem.Instance.AddToInventory(bp.bp_name);

        StartCoroutine(calculate());

    }

    public IEnumerator calculate()
    {
        yield return 0;
        InventorySystem.Instance.ReCalculateList();
        RefreshNeededItems();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Q) && !isOpen)
        {

            Debug.Log("tab is pressed");
            craftingScreenUI.SetActive(true);
            isOpen = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            SelectionManager.Instance.DisableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;

        }
        else if (Input.GetKeyDown(KeyCode.Q) && isOpen)
        {
            craftingScreenUI.SetActive(false);
            toolsScreenUI.SetActive(false);

            if (!InventorySystem.Instance.isOpen)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SelectionManager.Instance.EnableSelection();
                SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
            }

            isOpen = false;
            
        }
    }

    public void RefreshNeededItems()
    {

        rock_count = 0;
        branch_count = 0;

        inventoryItemList = InventorySystem.Instance.itemList;

        foreach (string itemName in inventoryItemList)
        {
            switch (itemName)
            {
                case "Rock":
                    rock_count += 1;
                    break;
                case "Branch":
                    branch_count += 1;
                    break;
            }
        }

        //----- AXE ------//

        AxeReq1.text = "Rock     [" + rock_count + "/3]";
        AxeReq2.text = "Branch   [" + branch_count + "/3]";

        if (rock_count >= 3 && branch_count >= 3)
        {
            craftAxeBTN.gameObject.SetActive(true);
        }
        else
        {
            craftAxeBTN.gameObject.SetActive(false);
        }

    }
}
