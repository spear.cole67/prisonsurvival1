using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Blueprint
{
    public string bp_name;
    public string req1;
    public string req2;

    public int req1Amount;
    public int req2Amount;

    public int reqAmount;

    public Blueprint(string n, string r1, string r2, int ra1, int ra2, int ra)
    {
        bp_name = n;
        req1 = r1;
        req2 = r2;

        req1Amount = ra1;
        req2Amount = ra2;
        reqAmount = ra;
    }

}
