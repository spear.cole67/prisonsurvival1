using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState: MonoBehaviour
{

    public static PlayerState Instance { get; set; }

    // ---- Player Health ---- //
    public float currentHealth, maxHealth;

    // ---- Player Calories ---- //
    public float currentCalories, maxCalories;

    float distanceTraveled = 0;
    Vector3 lastPosition;

    public GameObject playerBody;

    // ---- Player Hydration ---- //
    public float currentHydrationPercent, maxHydrationPercent;
    public bool isHydrationActive;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void setHealth(float newHealth) 
    {
        currentHealth = newHealth;
    }
    public void setCalories(float newCalories) 
    {
        currentCalories = newCalories;
    }
    public void setHydration(float newHydration) 
    {
        currentHydrationPercent = newHydration;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        currentCalories = maxCalories;
        currentHydrationPercent = maxHydrationPercent;

        isHydrationActive = true;

        StartCoroutine(dehydrate());
    }

    IEnumerator dehydrate()
    {
        while(isHydrationActive)
        {
            currentHydrationPercent -= 1;
            yield return new WaitForSeconds(10);
        }
    }

    // Update is called once per frame
    void Update()
    {
        distanceTraveled += Vector3.Distance(playerBody.transform.position, lastPosition);
        lastPosition = playerBody.transform.position;

        if(distanceTraveled >= 10)
        {
            distanceTraveled = 0;
            currentCalories -= 1;
        }
    }
}
